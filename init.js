/*----------------------------------------------
    
    tumblr polls - get start & end time
    - written by HT (@glenthemes)
    
----------------------------------------------*/

// DATE RESOURCES USED:
// epochconverter.com/programming/#javascript
// w3schools.com/jsref/jsref_obj_date.asp

document.addEventListener("DOMContentLoaded", () => {
    let js_npf = document.querySelectorAll("textarea[js-npf]");
    js_npf ? js_npf.forEach(npf_pseudo => {
        npf_data = npf_pseudo.value;
        if(npf_data && npf_data !== "null" && npf_data.trim() !== ""){
            
            npf_data = JSON.parse(npf_data);
            //console.log(npf_data)
            
            let npf_deets;
            
            /*-------------------*/
            
            // post: is original
            if(npf_data.trail.length == 0){
                npf_deets = npf_data.content[0];
            }
            
            // post: is reblog
            else if(npf_data.content.length == 0){
                let reblogs = npf_data.trail;
                
                // go through each reblog
                // find one that has a poll
                for(let reblog of reblogs){
                    for(let content of reblog.content){
                        if(content.type == "poll"){
                            npf_deets = content
                        }
                    }
                }
            }
            
            /*-------------------*/
            
            if(npf_deets.hasOwnProperty("type") && typeof(npf_deets.type) !== "undefined" && npf_deets.type == "poll"){
                
                // get year
                function epoch_year(epoch){
                    return epoch.getUTCFullYear();
                }
                
                // get month (short)
                function epoch_month(epoch){
                    let shortMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                    return shortMonths[epoch.getUTCMonth()];
                }
                
                // get day of month
                function epoch_day(epoch){
                    let res_day = epoch.getUTCDate();
                    res_day.toString().length == 1 ? res_day = "0" + res_day : "";
                    return res_day
                }
                
                // get hour
                function epoch_hr(epoch){
                    let res_hour = epoch.getUTCHours();
                    res_hour.toString().length == 1 ? res_hour = "0" + res_hour : "";
                    return res_hour
                }
                
                // get minutes
                function epoch_mins(epoch){
                    let res_mins = epoch.getUTCMinutes();
                    res_mins.toString().length == 1 ? res_mins = "0" + res_mins : "";
                    return res_mins
                }
                
                /*-----------------*/
                
                // poll start date
                let poll_start = npf_deets.created_at;
                let poll_start_epoch = Number(npf_deets.timestamp);
                let poll_start_epoch_prsd = new Date(poll_start_epoch * 1000);
                
                let poll_start_str = epoch_month(poll_start_epoch_prsd) + " " + epoch_day(poll_start_epoch_prsd) + ", " + epoch_year(poll_start_epoch_prsd) + ", " + epoch_hr(poll_start_epoch_prsd) + ":" + epoch_mins(poll_start_epoch_prsd) + " (UTC)";
                
                /*-----------------*/
                
                // poll end date
                let poll_end_epoch = Math.floor(Number(poll_start_epoch) + Number(npf_deets.settings.expire_after));
                let poll_end_epoch_prsd = new Date(poll_end_epoch * 1000);
                
                let poll_end_str = epoch_month(poll_end_epoch_prsd) + " " + epoch_day(poll_end_epoch_prsd) + ", " + epoch_year(poll_end_epoch_prsd) + ", " + epoch_hr(poll_end_epoch_prsd) + ":" + epoch_mins(poll_end_epoch_prsd) + " (UTC)";
                
                /*-----------------*/
                
                // today's date
                let poll_status = "ongoing";
                let today_epoch = Math.round(Date.now() / 1000);
                // Date.now() fsr is in milliseconds
                
                // if current date is past poll's assigned end,
                if(Number(today_epoch) > Number(poll_end_epoch)){
                    poll_status = "ended"
                }
                
                // print stuff out in case
                // console.log("POLL STARTED: " + poll_start_epoch)
                // console.log("POLL END(ED)(S): " + poll_end_epoch)
                // console.log("TODAY: " + today_epoch)
                // console.log("POLL STATUS " + poll_status)
                
                /*-----------------*/
                
                // see if .next() contains poll child
                let div_next = npf_pseudo.nextElementSibling;
                while(div_next){
                    let has_poll = div_next.querySelector("[data-npf*='\"type\":\"poll\"']");
                    if(has_poll){
                        // "poll started on..."
                        let poll_start_msg = document.createElement("div");
                        poll_start_msg.classList.add("poll-date-start");
                        poll_start_msg.textContent = "Started on " + poll_start_str;
                        
                        let poll_end_msg = document.createElement("div");
                        poll_end_msg.classList.add("poll-date-end");
                        
                        // poll ongoing
                        if(poll_status == "ongoing"){
                            poll_end_msg.textContent = "Ends on " + poll_end_str;
                        }
                        
                        // poll ended
                        else if(poll_status == "ended"){
                            poll_end_msg.textContent = "Ended on " + poll_end_str;
                        }
                        
                        
                        // for some reason .poll-row:last-of-type
                        // was not fuckin workin
                        if(has_poll.querySelector(".poll-see-results")){
                            has_poll.querySelector(".poll-see-results").before(poll_start_msg)
                            has_poll.querySelector(".poll-see-results").before(poll_end_msg)
                        } else {
                            has_poll.querySelector(".poll-row + .poll-row:last-of-type").after(poll_end_msg);
                            has_poll.querySelector(".poll-row + .poll-row:last-of-type").after(poll_start_msg);
                            
                            // if poll has ended,
                            // but "See Results" link doesn't exist,
                            // add it as a link
                            if(poll_status == "ended"){
                                let getLink = has_poll.querySelector("a.poll-row[href]").getAttribute("href");
                                let makeLink = document.createElement("a");
                                makeLink.href = getLink;
                                makeLink.rel = "nofollow";
                                makeLink.classList.add("poll-see-results");
                                makeLink.textContent = "See Results";
                                has_poll.append(makeLink)
                            }
                        }
                        
                        break
                    }
                    
                    div_next = div_next.nextElementSibling
                }
            }
        }
    }) : ""
    
});//end ready
