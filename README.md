### Tumblr Mod 「 polls 」  show start and end dates

**About:** Tumblr introduced [polls](https://staff.tumblr.com/post/706805192704802816) in Jan 2023. This add-on helps display the start date and end date for polls on custom themes (typically only displayed on the dashboard).   
**Author:** HT (@&#x200A;glenthemes)  
**View on Tumblr:** TBA

#### Features:
- shows when the poll **started** (e.g. `Feb 12, 2023, 18:38 (UTC)`)
- shows when the poll **should end** (e.g. `Feb 09, 2033, 18:38 (UTC)`)
- if the poll has ended, the "**See Results**" link will always show.

#### How it works:
This tool utilizes a JSON-parsed `{NPF}` variable and searches for any instances of polls. It retrieves the epoch `timestamp` of when it was created, and the `expire_after` value of when the original poster has set the poll to end and compares it to the current date and time.

#### How to use:

Place the following code after `<head>` or before `</head>`:

```html
<!--✻✻✻✻✻✻  SHOW POLL DATES by @glenthemes  ✻✻✻✻✻✻-->
<script src="//dash-polls.gitlab.io/show-dates/init.js"></script>
```

Look up `{block:Posts` in your theme, and paste this line *after* it:
```html
<textarea js-npf hidden aria-hidden="true">{NPF}</textarea>
```
